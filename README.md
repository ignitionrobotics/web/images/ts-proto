# TypeScript-Protobuf Docker image
This repository builds a docker image for generating proto messages in TypeScript using the following protoc plug-in: https://github.com/stephenh/ts-proto